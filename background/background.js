"use strict";

const main = () => {
	const contextMenuId = "doubleClickImageDownloader_DownloadImage";

	const onClicked = (info, tab) => {
		if (info.menuItemId === contextMenuId) {
			const options = {
				url: info.srcUrl,
				conflictAction: "uniquify"
			};

			chrome.downloads.download(options, downloadId => {
				chrome.storage.local.get({
					downloads: {}
				}, data => {
					data.downloads["dl" + downloadId] = {
						tab: tab.id
					};
					chrome.storage.local.set(data);
				});
			});
		}
	};

	const onMessage = function(msg, sender, sendResponse) {
		if (msg.subject === "downloadRequested") {
			const options = {
				url: msg.url,
				conflictAction: "uniquify"
			};

			chrome.downloads.download(options, downloadId => {
				sendResponse({
					id: downloadId
				});

				chrome.storage.local.get({
					downloads: {}
				}, data => {
					data.downloads["dl" + downloadId] = {
						tab: sender.tab.id
					};
					chrome.storage.local.set(data);
				});
			});

			return true;
		}
	};

	const onDeterminingFilename = (downloadItem, suggest) => {
		chrome.storage.sync.get({
			enableRename: false,
			fileNamePattern: "%original%",
			counterPadding: 1,
			enableSubfolder: false,
			subfolderNamePattern: "%imagedomain%"
		}, options => {
			chrome.storage.local.get({
				downloads: {},
				downloadCounter: 1
			}, data => {
				if (options.enableRename) {
					chrome.storage.local.set({
						downloadCounter: data.downloadCounter + 1
					});
				}

				const tab = data.downloads["dl" + downloadItem.id];
				if (!tab) {
					suggest();
				}
				const tabId = tab.tab;

				chrome.tabs.get(tabId, tab => {
					if (options.enableRename || options.enableSubfolder) {
						const file = {
							name: "",
							extension: ""
						};
						const periodIndex = downloadItem.filename.lastIndexOf(".");

						if (~periodIndex) {
							file.name = downloadItem.filename.substring(0, periodIndex);
							file.extension = downloadItem.filename.substring(periodIndex);
						} else {
							file.name = downloadItem.filename;
						}

						const imageUrl = new URL(downloadItem.url);
						const tabUrl = new URL(tab.url);

						if (options.enableRename) {
							file.name = options.fileNamePattern
								.replace(/%original%/g, file.name)
								.replace(/%pagedomain%/g, tabUrl.hostname)
								.replace(/%imagedomain%/g, imageUrl.hostname)
								.replace(/%title%/g, tab.title)
								.replace(/%counter%/g, (() => {
									let out = String(data.downloadCounter);
									return "0".repeat(Math.max(0, options.counterPadding - out.length)) + out;
								})())
							;
						}
						if (options.enableSubfolder) {
							file.name = options.subfolderNamePattern
								.replace(/%pagedomain%/g, tabUrl.hostname)
								.replace(/%imagedomain%/g, imageUrl.hostname)
								.replace(/%title%/g, tab.title)
							+ "/" + file.name;
						}

						suggest({
							filename: file.name + file.extension,
							conflictAction: "uniquify"
						});
					} else {
						suggest();
					}
				});
			});
		});

		return true;
	};

	const onChangedDownloadFound = downloadItems => {
		const downloadItem = downloadItems[0];
		// TODO firefox support
		try {
			chrome.notifications.create({
				type: "image",
				title: "Image Downloaded",
				message: downloadItem.filename,
				iconUrl: "/images/icon-128.png",
				imageUrl: downloadItem.filename,
				buttons: [{
					title: "View image"
				}, {
					title: "Open folder"
				}]
			}, notificationId => {
				chrome.storage.local.get({
					notifications: {}
				}, data => {
					data.notifications[notificationId] = {
						download: downloadItem.id
					};
					chrome.storage.local.set(data);
				});
			});
		} catch(e) {
			chrome.notifications.create({
				type: "basic",
				title: "Image Downloaded",
				message: downloadItem.filename,
				iconUrl: "/images/icon-128.png"
			});
		}
	};

	const onDownloadChanged = downloadDelta => {
		chrome.storage.sync.get({
			notify: true,
		}, options => {
			if (downloadDelta.state !== undefined && downloadDelta.state.current === "complete") {
				chrome.storage.local.get({
					downloads: {}
				}, data => {
					const tab = data.downloads["dl" + downloadDelta.id];
					if (!tab) {
						return;
					}
					const tabId = tab.tab;

					delete data.downloads["dl" + downloadDelta.id];
					chrome.storage.local.set(data);

					chrome.tabs.sendMessage(tabId, {
						subject: "downloadFinished",
						id: downloadDelta.id
					});
				});

				if (options.notify === true) {
					chrome.downloads.search({
						id: downloadDelta.id
					}, onChangedDownloadFound);
				}
			}
		});
	};

	const onButtonClicked = (notificationId, buttonIndex) => {
		chrome.storage.local.get({
			notifications: {}
		}, data => {
			const id = data.notifications[notificationId].download;
			switch (buttonIndex) {
				case 0:
					chrome.downloads.open(id);
					break;

				case 1:
					chrome.downloads.show(id);
					break;
			}
		});
	};

	const onNotificationClosed = (notificationId) => {
		chrome.storage.local.get({
			notifications: {}
		}, data => {
			delete data.notifications[notificationId];
			chrome.storage.local.set(data);
		});
	};

	chrome.runtime.onMessage.addListener(onMessage);
	// TODO firefox support
	if (chrome.downloads.onDeterminingFilename !== undefined) {
		chrome.downloads.onDeterminingFilename.addListener(onDeterminingFilename);
	}
	chrome.downloads.onChanged.addListener(onDownloadChanged);
	chrome.notifications.onButtonClicked.addListener(onButtonClicked);
	chrome.notifications.onClosed.addListener(onNotificationClosed);
	chrome.contextMenus.onClicked.addListener(onClicked);

	const onCreate = () => {
		const error = chrome.runtime.lastError != null? chrome.runtime.lastError.message: null;
		if (error === "Cannot create item with duplicate id " + contextMenuId) {
			// ignore
		} else if (error != null) {
			throw new Error(error);
		}
	};

	chrome.contextMenus.create({
		type: "normal",
		id: contextMenuId,
		title: "Download image",
		contexts: ["image"],
		documentUrlPatterns: ["*://*/*", "file:///*"]
	}, onCreate);
};

chrome.runtime.onStartup.addListener(() => {
	chrome.storage.local.set({
		downloads: {},
		notifications: {},
		downloadCounter: 1
	});
});
